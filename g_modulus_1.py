# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 09:35:01 2024
Project_nam: Calculator_Gitlab
Script: Modulus
@author: Karthik
"""

def modulus(x, y):
    """This function returns the remainder of x divided by y"""
    return x % y

# num1 = float(input("Enter first number: "))
# num2 = float(input("Enter second number: "))

# print("Result:", modulus(num1, num2))
