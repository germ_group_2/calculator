# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 09:35:01 2024
Project_nam: Calculator_Gitlab
Script: Main
@author: Karthik
"""

import a_addition
import b_subtraction
import c_multiplication
import d_division
import e_exponentiation
import f_squareroot
import g_modulus

print("Select operation:")
print("1. Addition")
print("2. Subtraction")
print("3. Multiplication")
print("4. Division")
print("5. Exponentiation")
print("6. Square Root")
print("7. Modulus")


while True:
    choice = input("Enter choice (1/2/3/4/5/6/7): ")

    if choice == '1':
        num1 = float(input("Enter first number: "))
        num2 = float(input("Enter second number: "))
        print("Result:", a_addition.add(num1, num2))
    elif choice == '2':
        num1 = float(input("Enter first number: "))
        num2 = float(input("Enter second number: "))
        print("Result:", b_subtraction.subtract(num1, num2))
    elif choice == '3':
        num1 = float(input("Enter first number: "))
        num2 = float(input("Enter second number: "))
        print("Result:", c_multiplication.multiply(num1, num2))
    elif choice == '4':
        num1 = float(input("Enter dividend: "))
        num2 = float(input("Enter divisor: "))
        print("Result:", d_division.divide(num1, num2))
    elif choice == '5':
        num1 = float(input("Enter base number: "))
        num2 = float(input("Enter exponent: "))
        print("Result:", e_exponentiation.exponentiation(num1, num2))
    elif choice == '6':
        num = float(input("Enter number: "))
        print("Result:", f_squareroot.square_root(num))
    elif choice == '7':
        num1 = float(input("Enter first number: "))
        num2 = float(input("Enter second number: "))
        print("Result:", g_modulus.modulus(num1, num2))
    else:
        print("Invalid input")

    next_calculation = input("Do you want to perform another calculation? (yes/no): ")
    if next_calculation.lower() != 'yes':
        break
